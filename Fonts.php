<?php
namespace samson\fonts;

use samson\core\CompressableExternalModule;

/**
 * Fonts module
 * 
 * @package SamsonPHP
 * @author Vitaly Iegorov <vitalyiegorov@gmail.com> 
 * @version 0.2
 */ 
class Fonts extends CompressableExternalModule
{
    /** Реальный идентификатор файлов модуля */
    protected $id = 'fonts';

    /** Автор */
    protected $author = 'Vitaly Iegorov';

    public function __googlefont($font)
    {
        s()->async(true);
        // Replace spaces
        $font = str_replace(' ', '+', $font);
        // Pass to font CSS generator
        $url = 'http://fonts.googleapis.com/css?family='.$font;

        // List of main http browser agents
        $agents = array(
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12',
            '',
            'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
            //'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
        );
        // Collection of google responses
        $response = array();

        // Iterate agents
        foreach ( $agents as $agent ) {

            $ch = curl_init();

            $header=array(
                'User-Agent: '.$agent,
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language: en-us,en;q=0.5',
                'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
                'Keep-Alive: 115',
                'Connection: keep-alive',
            );

            curl_setopt($ch,CURLOPT_URL,$url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_HTTPHEADER,$header);

            $response[] = curl_exec($ch);

            curl_close($ch);
        }

        $pattern = '/@font-face\s*{\s*font-family:\s*\'(?P<family>[^\']+)\';\s*font-style:(?P<style>[^;]+);\s*font-weight:(?P<weight>[^;]+);\s*src:(?P<src>[^;]+)/smiu';
        $css = array();

        // Iterate responses
        foreach ($response as $r) {
            // Parse @font-face
            if (preg_match_all($pattern, $r, $matches)) {

                // Iterate found font-faces
                for($i=0; $i<sizeof($matches['src']); $i++) {

                    // Get font name
                    $family = str_replace(' ', '', $matches['family'][$i]);

                    // Create font key
                    $key = $family.$matches['style'][$i].$matches['weight'][$i];

                    // Create font record
                    if (!isset($css[$key])) {
                        $css[$key] = array();
                        $css[$key]['family'] = $family;
                        $css[$key]['style'] = $matches['style'][$i];
                        $css[$key]['weight'] = $matches['weight'][$i];
                        $css[$key]['src'] = array();
                    }

                    // Array of local pathes to font files
                    $src = & $css[$key]['src'];

                    // Build path to font directory
                    $path = $this->path().'fonts/'.$family.'/';

                    // Create font path
                    if (!file_exists($path)) {
                        mkdir($path, 0777,true);
                    }

                    // Split font files urls
                    $urls = explode(',', $matches['src'][$i]);

                    // Iterate splitted font urls
                    foreach ($urls as $url) {
                        // If thi is font file url
                        if (preg_match('/\s*url\((?P<url>[^\)]+)\)\s*(?P<format>[^,]+)*/', $url, $matches2)) {

                            // Get only file name
                            $file = basename($matches2['url']);

                            // Load font file if it does not exists
                            if (!file_exists($path.$file)) {
                                file_put_contents($path.$file, file_get_contents($matches2['url']));
                            }

                            // Build CSS src string
                            $src[pathinfo($file, PATHINFO_EXTENSION)] = 'url(\'fonts/'.$family.'/'.$file.'\') '.(isset($matches2['format'])?$matches2['format']:'');

                        } elseif(!in_array($url, $src)) {
                            //$src[] = $url;
                        }
                    }
                }
            }
        }

        $result = '';
        $font = null;
        // Generate font css file
        foreach ($css as $font) {
            $result .= "\n".'@font-face {';
            $result .= "\n\t"."font-family: '".trim($font['family'])."';";
            $result .= "\n\t"."font-style: ".trim($font['style']).";";
            $result .= "\n\t"."font-weight: ".trim($font['weight']).";";
            $result .= "\n\t"."src: ".trim($font['src']['eot']).";";
            $result .= "\n\t"."src: ".trim(implode(', ', $font['src'])).";";
            $result .= "\n"."}";
        }

        // Build path to font directory
        $path = $this->path().'fonts/'.$font['family'].'/stylesheet.css';

        file_put_contents($path, $result);

        trace($css, true);
    }
}